const book = require('../models/book');
const {Book} = require('../models');
const { response } = require('express');
module.exports = {
    index: (req,res) => {
        const title = "Awal";
        Book.findAll().then((book) => {
            if(book !=0 ){
                res.json({
                    'status': 200,
                    'message': 'berhasil',
                    'data': book
                })
            } else {
                res.json({
                    'status': 400,
                    'message': 'data tidak ditemukan'
                })
            } 
        }).catch(err => {
            res.json({
                'status': 500,
                'message': 'kesalahan server'
            })
        })
    },
    detail: (req,res) => {
        const title = "Details";
        const articleId = req.params.id
        Book.findOne({
            where: {
                id : articleId
            }
        }).then(book => {
            res.json({
                'status': 200,
                'message': 'berhasil',
                'data': book
            })
        }).catch(err => {
            res.json({
                'status': 500,
                'message': 'kesalahan server'
            })
        })
    },
    create: (req,res) => {
        const {isbn, judul, sinopsis, penulis, genre} = req.body;
        Book.create({
            isbn,
            judul,
            sinopsis,
            penulis,
            genre
        }).then(book => {
            console.log(book),
            res.json({
                'status': 200,
                'message': 'post berhasil dibuat',
                'data': book
            })
        }).catch(err => {
            res.json({
                'status': 400,
                'message': 'POST Gagal membuat',
                'error': err.message
            })
        })
    },
    update: (req,res) => {
        const id = req.params.id;
        Book.update({
            isbn: req.body.isbn,
            judul: req.body.judul,
            sinopsis: req.body.sinopsis,
            penulis: req.body.penulis,
            genre: req.body.genre
        }, {
            where:{
                id: req.params.id
            }
        }).then(book => {
            res.status(201).json(book)
        }).catch(err => {
            res.status(422).json({status: 'error'})
        })
    },
    delete: (req,res) => {
        const articleId = req.params.id
        Book.destroy({
            where: {
                id: articleId
            }
        }).then(response =>{
            res.json({
                'status':200,
                'message':'sudah dihapus bre'
            })
        }).catch(err => {
            res.json({
                'status':400,
                'message':'delete gagal'
            })
        })
    }
};
