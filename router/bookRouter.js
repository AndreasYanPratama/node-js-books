const router = require('express').Router();
const bookController = require('../controller/bookController');
router.get('/',bookController.index);

router.get('/:id',bookController.detail);
router.post('/create',bookController.create);
router.put('/:id',bookController.update);
router.delete('/:id',bookController.delete);

module.exports = router;