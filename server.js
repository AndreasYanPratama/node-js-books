const express = require('express');
const app = express();
const port = 3000;//port ini bebas (boleh 5000, dll), yg penting yg blm kepakai
// app.get('/',(req,res) => res.send('Belajar express'));
// app.get('/articles',(req,res) => res.send('Ini adalah Articles'));
const router = require('./router');

app.use(express.urlencoded({ extended : false }));
app.use(express.json());
app.set('view engine','ejs');

//import router ModelControllerRouter
app.use(router);

//call web server
app.listen(port, () => {
    console.log(`Server sudah berjalan di http://localhost:${port}`)
});

